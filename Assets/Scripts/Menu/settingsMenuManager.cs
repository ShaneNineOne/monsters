﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class settingsMenuManager : MonoBehaviour {

    /// <summary>
    /// All the enemy UI buttons
    /// </summary>
    [SerializeField] private Button[] enemyButtons;

    /// <summary>
    /// All the key UI buttons
    /// </summary>
    [SerializeField] private Button[] keyButtons;

    /// <summary>
    /// The index of the currently selected enemy button from enemyButtons
    /// </summary>
    private int selectedEnemyButton;

    /// <summary>
    /// The index of the currently selected key button from keyButtons
    /// </summary>
    private int selectedKeyButton;

    private Color highlightedColor = new Color(0.6f, 0, 0, 1);

    void Start()
    {
        selectedEnemyButton = gameSettings.settings.numOfEnemies - 1;
        selectedKeyButton = gameSettings.settings.numOfKeys - 1;
        highlightEnemyButton();
        highlightKeyButton();
    }

    /// <summary>
    /// Handling for pressing the enemy button in the settings menu.
    /// </summary>
    /// <param name="num">How many enemies did the player choose to spawn?</param>
    public void enemyButtonPressed(int num)
    {
        gameSettings.settings.setNumOfEnemies(num);
        highlightEnemyButton();
    }

    /// <summary>
    /// Handling for pressing the key button in the settings menu.
    /// </summary>
    /// <param name="num">How many keys did the player choose to spawn?</param>
    public void keyButtonPressed(int num)
    {
        gameSettings.settings.setNumOfKeys(num);
        highlightKeyButton();
    }

    /// <summary>
    /// Unhighlights the currently highlighted enemy button and highlights the one the player just chose.
    /// </summary>
    private void highlightEnemyButton()
    {
        enemyButtons[selectedEnemyButton].image.color = Color.black;
		//Ugly, but this reduces our odd numbers (3, 5, 7, 9, 11) into their sequential button numbers (0, 1, 2, 3, 4) and spares us iterating through if statements.
		selectedEnemyButton = (int) ((gameSettings.settings.numOfEnemies * 0.5f) - 1.5f);
        enemyButtons[selectedEnemyButton].image.color = highlightedColor;
    }

    /// <summary>
    /// Unhighlights the currently highlighted key button and highlights the one the player just chose.
    /// </summary>
    private void highlightKeyButton()
    {
        keyButtons[selectedKeyButton].image.color = Color.black;
        selectedKeyButton = gameSettings.settings.numOfKeys - 1;
        keyButtons[selectedKeyButton].image.color = highlightedColor;
    }
}
