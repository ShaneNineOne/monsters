﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DebugController : MonoBehaviour {

    public bool debugModeOn;
	
	// Update is called once per frame
	void Update () {
        if (!debugModeOn) return;

        //Toggle fog on and off
        if (Input.GetKeyDown(KeyCode.F))
        {
            RenderSettings.fog = !RenderSettings.fog;
        }

        //Collect all keys
        if (Input.GetKeyDown(KeyCode.K))
        {
            //Set our keys to the maximum value and destroy all remaining active keys on the field.
            keyManager.keyManagement.currentKeyCount = keyManager.keyManagement.numOfKeys;
            foreach (GameObject newKey in GameObject.FindGameObjectsWithTag("key"))
            {
                Destroy(newKey);
            }
            keyManager.keyManagement.allKeysGathered();//Handle all keys gathered event

        }

        //Instantly die
        if (Input.GetKeyDown(KeyCode.N))
        {
            SceneManager.LoadScene("deathScene");
        }

    }

    void OnGUI()
    {
        if (debugModeOn)
        {
            GUI.color = new Color(0.4f, 0, 0.8f);
            GUI.Label(new Rect(Screen.width - 150, 20, 200, 20), "DEBUG COMMANDS:");
            GUI.Label(new Rect(Screen.width - 150, 35, 200, 22), "F: Toggle fog on/off");
            GUI.Label(new Rect(Screen.width - 150, 50, 200, 20), "K: Collect all keys");
            GUI.Label(new Rect(Screen.width - 150, 65, 200, 20), "N: Go to death scene");
        }
    }
}
