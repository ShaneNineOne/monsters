﻿using UnityEngine;
using System.Collections;

public class DeathMusicController : MonoBehaviour {


    [SerializeField] private AudioSource source;

    void Awake()
    {
        source.Play();
    }

}
