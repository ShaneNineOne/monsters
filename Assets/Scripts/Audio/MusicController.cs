﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

    /// <summary>
    /// The music player.
    /// </summary>
    [SerializeField] private AudioSource musicPlayer;

    /// <summary>
    /// The collection of audio tracks we have to play.
    /// </summary>
    [SerializeField] private AudioClip[] audioTracks;

    /// <summary>
    /// Static reference so we can access the music controller from any other script in the scene.
    /// </summary>
    public static MusicController musicControl;

    void Awake()
    {
        musicControl = this;
        musicPlayer = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Start playing a new track immediately, looping the track once it has finished playing.
    /// </summary>
    /// <param name="trackIndex">The index of the audio track to play.</param>
    public void startNewLoopableTrack(int trackIndex)
    {
        setTrack(trackIndex);
        setRepeatOn();
        playCurrentTrack();
    }

    /// <summary>
    /// Start playing a new track immediately, but do not loop the track once it has finished playing.
    /// </summary>
    /// <param name="trackIndex">The index of the audio track to play.</param>
    public void startNewNonLoopableTrack(int trackIndex)
    {
        setTrack(trackIndex);
        setRepeatOff();
        playCurrentTrack();
    }

    /// <summary>
    /// Change the currently loaded audio clip to the clip specified by the index.
    /// </summary>
    /// <param name="index">The index of the audio clip to load.</param>
    private void setTrack(int index)
    {
        musicPlayer.clip = audioTracks[index];
    }

    /// <summary>
    /// Plays the current audio track.
    /// </summary>
    private void playCurrentTrack()
    {
        musicPlayer.Play();
    }

    /// <summary>
    /// Sets the audio player to loop the current track when it finishes playing.
    /// </summary>
    private void setRepeatOn()
    {
        musicPlayer.loop = true;
    }

    /// <summary>
    /// Sets the audio player to not loop the current track when it finishes playing.
    /// </summary>
    private void setRepeatOff()
    {
        musicPlayer.loop = false;
    }
}
