﻿using UnityEngine;
using System.Collections;

public class CreepySoundController : MonoBehaviour {

    [SerializeField] private AudioSource source;
    private bool isPlayed;

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && !isPlayed) {
            source.Play();
            isPlayed = !isPlayed;
        }
    }
}
