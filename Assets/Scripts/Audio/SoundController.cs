﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

    /// <summary>
    /// The music player.
    /// </summary>
    [SerializeField]
    private AudioSource soundPlayer;

    /// <summary>
    /// The collection of audio tracks we have to play.
    /// </summary>
    [SerializeField]
    private AudioClip[] effectTracks;

    /// <summary>
    /// Static reference so we can access the music controller from any other script in the scene.
    /// </summary>
    public static SoundController soundControl;

    void Awake()
    {
        soundControl = this;
        soundPlayer = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Play the sound effect of the specified index
    /// </summary>
    /// <param name="effectIndex">The index of the sound effect</param>
    public void playSoundEffect(int effectIndex)
    {
        soundPlayer.PlayOneShot(effectTracks[effectIndex]);//One shot allows us to play multiple sound effects at once using one AudioSource.
    }
         
}
