﻿using UnityEngine;
using System.Collections;

public class openingScene : MonoBehaviour {

    //NOTE - This is a very poorly designed script. This is limited to one and only one cinematic sequence that is essentially hard coded in.
    //Were we to try making a more proper cinematic manager, we would want it to be more expandable with many kinds of actions that can be carried out at any time.
    //Provided we have time, we can overhaul this script to handle more varied cinematic events, such as: 
    //Text display on screen for events such as key pickups, death cinematic, escape cinematic, etc.

    /// <summary>
    /// What is the value we want the fog radius to end at?
    /// </summary>
    [SerializeField] private float finalFogRadiusValue;
    
    /// <summary>
    /// What is the initial fog radius value?
    /// </summary>
    [SerializeField] private float initialFogRadiusValue;

    /// <summary>
    /// What is the current value of the fog radius?
    /// </summary>
    private float currentFogRadiusValue;

    /// <summary>
    /// How long does the opening cinematic last?
    /// </summary>
    [SerializeField] private float openingDuration;

    /// <summary>
    /// The rate at which we decay the fog density per second.
    /// </summary>
    private float decayRate;

    /// <summary>
    /// The rate at which we decay the alpha for the intro text.
    /// </summary>
    private float alphaDecayRate;

    /// <summary>
    /// Is the opening cinematic completed?
    /// </summary>
    public bool openingComplete = false;

    public float startTime;

    public static openingScene openingCinematic;

    void Start()
    {
        startTime = Time.time;
        openingCinematic = this;
        decayRate = (initialFogRadiusValue - finalFogRadiusValue) / openingDuration;
        alphaDecayRate = 1 / openingDuration;

        MusicController.musicControl.startNewLoopableTrack(0);
    }

    void Update()
    {
        if (openingComplete) return;

        currentFogRadiusValue = initialFogRadiusValue - (decayRate * (Time.time - startTime));
        RenderSettings.fogDensity = currentFogRadiusValue;

        if (currentFogRadiusValue <= finalFogRadiusValue)
        {
            openingComplete = true;
        }
    }

    void OnGUI()
    {
        if (openingComplete) return;

        //Some stylization to make the text look nicer.
        GUIStyle centeredStyle = new GUIStyle();
        centeredStyle.fontStyle = FontStyle.Bold;
        centeredStyle.alignment = TextAnchor.UpperCenter;
        centeredStyle.fontSize = 30;
        centeredStyle.normal.textColor = new Color(0.4f, 0, 0.8f, (1 - (alphaDecayRate * (Time.time - startTime))));//We decay the alpha here across the timespan of the duration... we want it to fade into nothingness the moment the player gains control.
        GUI.Label(new Rect(Screen.width/2 - 150/2, Screen.height/2, 150, 50), "FIND KEYS AND SEEK AN EXIT!", centeredStyle);//Fancy math here to get the label to display in the center of the screen.
    }
}
