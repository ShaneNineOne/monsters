﻿using UnityEngine;
using System.Collections;

public class gameSettings : MonoBehaviour {

    public static gameSettings settings;

    /// <summary>
    /// How many enemies should we spawn?
    /// </summary>
    public int numOfEnemies = 3;

    /// <summary>
    /// How many keys should we spawn?
    /// </summary>
    public int numOfKeys = 3;

    void Awake()
    {
        //We only ever want one copy of settings around at a time.
        if (settings)
        {
            Destroy(this.gameObject);
        } else
        {
            settings = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void setNumOfKeys(int num)
    {
        numOfKeys = num;
    }

    public void setNumOfEnemies(int num)
    {
        numOfEnemies = num;
    }
}
