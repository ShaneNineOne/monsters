﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class enemySpawner : MonoBehaviour {

    //The list of points we can choose to move to
    public List<Transform> targets;

    public int numOfEnemies;

    public Transform enemy;

	// Use this for initialization
	void Start () {
        numOfEnemies = gameSettings.settings.numOfEnemies;

        foreach (GameObject position in GameObject.FindGameObjectsWithTag("spawnTarget"))
        {
            targets.Add(position.transform);
        }

        for (int x = 0; x < numOfEnemies; x++)
        {
            //Spawn the enemy at a random spawn point (It's okay if they spawn at the same location... enemies phase through one another)
            int target = Random.Range(0, targets.Count);
            Transform newEnemy = Instantiate(enemy, new Vector3(targets[target].position.x, 0.5f, targets[target].position.z), Quaternion.identity) as Transform;
            newEnemy.SetParent(transform.parent);//Purely for editor organization... we want to set the parent object to the parent of our spawner, so that the enemies end up in the same location as our spawner for editor convenience.
        }
	}
}
