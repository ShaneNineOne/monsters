﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class keyManager : MonoBehaviour {

    public Transform[] targets;

    public Transform key;

    public int numOfKeys;

    public int currentKeyCount = 0;

    public static keyManager keyManagement;

    [SerializeField]
    private GUIStyle keyTextStyle;

    // Use this for initialization
    void Start () {
        numOfKeys = gameSettings.settings.numOfKeys;

        keyManagement = this;

        List<int> chosenTargets = new List<int>();
        for (int x = 0; x < numOfKeys; x++)
        {
            int target = Random.Range(0, targets.Length);
            //We can't reuse the same position... try again.
            if (chosenTargets.Contains(target))
            {
                x--;
            } else
            {
                chosenTargets.Add(target);
                Transform newKey = Instantiate(key, new Vector3(targets[target].position.x, 0.5f, targets[target].position.z), Quaternion.identity) as Transform;
                newKey.SetParent(transform.parent);
            }
        }
    }

    /// <summary>
    /// Handles the events that occur upon gathering all keys.
    /// </summary>
    public void allKeysGathered()
    {
        if (keyManager.keyManagement.currentKeyCount < keyManager.keyManagement.numOfKeys) return;
        MusicController.musicControl.startNewLoopableTrack(1);
    }

    void OnGUI()
    {
        if (!openingScene.openingCinematic.openingComplete) return;

        GUI.color = new Color(0.4f, 0, 0.8f);
        GUI.Label(new Rect(20, 40, 200, 20), "Keys collected: " + currentKeyCount + "/" + numOfKeys, keyTextStyle);
    }

    /// <summary>
    /// Set up for a new game again...
    /// </summary>
    public void reset()
    {
        currentKeyCount = 0;
    }
}
