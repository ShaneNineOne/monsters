﻿using UnityEngine;
using System.Collections;

public class key : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
	if (other.CompareTag("Player")) {
        keyManager.keyManagement.currentKeyCount++;
        keyManager.keyManagement.allKeysGathered();//Handle all keys gathered event if we need to
        Destroy(this.gameObject);
	}
    }
}
