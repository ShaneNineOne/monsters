﻿using UnityEngine;
using System.Collections;

public class OpenDoor : MonoBehaviour {


	public GameObject player;

	private Animator anim;

	private bool unlock;


	// Use this for initialization
	void Awake () {
		
		anim = GetComponent<Animator> ();
		
	}

	void OnTriggerEnter(Collider other){
		//If the player has all of the keys, the door is unlocked
		if ((other.gameObject == player) && (keyManager.keyManagement.currentKeyCount == keyManager.keyManagement.numOfKeys)) {
			unlock = true;
		}

	}

	// Update is called once per frame
	void Update () {

		anim.SetBool("Open", unlock);

	}
}
