﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pathfinding : MonoBehaviour
{
    //TODO: This entire script is in need of some cleaning up...
    [SerializeField] private Transform target;

    public bool pathFound = false;
    public int currentWaypoint = 0;

    private EnemyController enemyControl;


    /// <summary>
    /// Should we draw the path when it has been found? (FOR DEBUG PURPOSES ONLY)
    /// </summary>
    public bool drawPath;
    //The list of points we can choose to move to
    public List<Transform> targets;

    //The last waypoint we targeted.
    private int lastTarget = 0;

    //The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance;

    public List<Node> path;

    Grid grid;

    void Awake()
    {
        //Populate the list of available targets...
        foreach (GameObject position in GameObject.FindGameObjectsWithTag("pathfindTarget"))
        {
            targets.Add(position.transform);
        }

        int targetPosition = Random.Range(0, targets.Count);
        enemyControl = GetComponent<EnemyController>();
        target = targets[targetPosition];
        lastTarget = targetPosition;
        grid = FindObjectOfType<Grid>();
    }

    void Update()
    {
        //Don't look for a new path unless we don't have a path found already AND we don't see the player in our LOS.
        if (!pathFound && !enemyControl.playerDetected && !enemyControl.searchingForPlayer)
        {
            //Debug.Log(transform.gameObject.name + " is finding a path...");
            int targetPosition = Random.Range(0, targets.Count);//Random target.

            //Check to see that we have more than one target, otherwise we will end up in an infinite loop if we have only one target...
            while (targets.Count > 1 && targetPosition == lastTarget)
            {
                targetPosition = Random.Range(0, targets.Count);//Change the target until it is a new one if it's one we've just been to.
            }
            lastTarget = targetPosition;

            FindPath(transform.position, targets[targetPosition].position);
        } else if (pathFound) {
            if (drawPath)
            {
                drawCurrentPath();
            }

            //Break free of the path either if we have gotten to the end, or if we have detected the player.
            if (currentWaypoint >= (path.Count - 1))
            {
                cancelPath();
                enemyControl.startSearchMode();//Start search mode
                Debug.Log(transform.gameObject.name + " has reached the end of its path.");
            } 
        }
    }

    /// <summary>
    /// Brings the current path to an end and sets up for a new path.
    /// </summary>
    public void cancelPath()
    {
        pathFound = false;
        currentWaypoint = 0;
    }

    /// <summary>
    /// Increments the current waypoint if necessary
    /// </summary>
    public void incrementCurrentWaypoint()
    {
        if (Vector3.Distance(transform.position, path[currentWaypoint].worldPosition) < nextWaypointDistance && currentWaypoint < path.Count)
        {
            currentWaypoint++;
        }
    }

    /// <summary>
    /// Finds a path to the closest walkable node to the player's current location.
    /// </summary>
    /// <param name="playerPos">The position of the player</param>
    public void findPathToPlayer(Vector3 playerPos)
    {
        Node playerNode = grid.findValidPlayerNode(playerPos);
        FindPath(transform.position, playerNode.worldPosition);
    }

    /// <summary>
    /// Finds a path via A* from our start position to our target position.
    /// </summary>
    /// <param name="startPos">The starting position of the AI.</param>
    /// <param name="targetPos">The target we are trying to pathfind our way to.</param>
    public void FindPath(Vector3 startPos, Vector3 targetPos)
    {
        //REMOVE THE TARGET POSITION STUFF FROM HERE. JUST CHANGE IT SO WE GENERATE THE NEW TARGET POSITION ON PATH DISCOVERY AND PASS IT IN AS targetPos.
        path.Clear();

        Node startNode = grid.NodeFromWorldPoint(startPos);
        Node targetNode = grid.NodeFromWorldPoint(targetPos);

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();
        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            Node node = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < node.fCost || openSet[i].fCost == node.fCost)
                {
                    if (openSet[i].hCost < node.hCost)
                        node = openSet[i];
                }
            }

            openSet.Remove(node);
            closedSet.Add(node);

            if (node == targetNode)
            {
                //Debug.Log(transform.gameObject.name + " has found a path!");
                RetracePath(startNode, targetNode);
                pathFound = true;
                return;
            }

            foreach (Node neighbor in grid.GetNeighbors(node))
            {
                if (!neighbor.walkable || closedSet.Contains(neighbor))
                {
                    continue;
                }

                int newCostToNeighbour = node.gCost + GetDistance(node, neighbor);
                if (newCostToNeighbour < neighbor.gCost || !openSet.Contains(neighbor))
                {
                    neighbor.gCost = newCostToNeighbour;
                    neighbor.hCost = GetDistance(neighbor, targetNode);
                    neighbor.parent = node;

                    if (!openSet.Contains(neighbor))
                        openSet.Add(neighbor);
                }
            }
        }
    }

    void RetracePath(Node startNode, Node endNode)
    {
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();
    }

    private void drawCurrentPath()
    {
        for (int x = 0; x < path.Count - 2; x++)
        {
            Debug.DrawLine(path[x].worldPosition, path[x + 1].worldPosition, Color.green);
        }
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }
}