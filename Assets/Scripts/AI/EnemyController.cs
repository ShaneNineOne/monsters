﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    //NOTES: Speed, LOS, etc. should be inherited from another monster type class that stores data on a per monster type basis.
    [SerializeField] private float speed;

    private Rigidbody Rb;

    private Pathfinding pathfinder;

    public Transform Player;

    public float maxLOS;

    public int enemyType;

    public EnemyData data;

    public bool playerDetected = false;

    public bool searchingForPlayer = false;

    /// <summary>
    /// Are we actively seeking the player after spotting him right now?
    /// </summary>
    public bool searchModeActive = false;

    public bool drawLOS;

    /// <summary>
    /// Which phase of the search sequence are we currently in?
    /// </summary>
    private int searchPhase = 1;

    private Quaternion searchLeft;

    private Quaternion searchRight;

    private float searchStartTime;

    private float defaultLOS = 0.6f;

    // Use this for initialization
    void Awake () {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        data = new EnemyData();
        Rb = GetComponent<Rigidbody>();
        pathfinder = GetComponent<Pathfinding>();
	}

    // Check to see if the player is within LOS. (Right now, this checks every frame - we might consider increasing this to a higher number of frams for the sake of performance.
    void Update() {

        //Can we delay LOS checks to every 5 or so frames? Something to consider for optimization.


        //NOTE FOR EVASION: We will need an additional state for obstacle avoidance. The logic should be simple... if we are avoiding an obstacle, only focus on that. Afterward, the monster will generate a new path to the player's position.

        if (playerDetected) {
           playerDetectedMove();
        } else if (checkLOS(defaultLOS)) {
            //Stop pathfinding - we're jumping into chase mode.
            Debug.Log("Player detected within LOS!");
            if (!searchingForPlayer)
            {
                SoundController.soundControl.playSoundEffect(0);//Play detection sound on first sighting.
            }

            pathfinder.cancelPath();
            playerDetected = true;
            searchingForPlayer = true;//Enter the search state.
            playerDetectedMove(); 

        } else if (searchModeActive) {
            searchMode();
        } else {
            pathfindingMove();
        }
    }

    //Sets all the appropriate variables to start the search phase.
    public void startSearchMode()
    {
        searchModeActive = true;
        searchPhase = 1;
        searchStartTime = Time.time;
        searchLeft = transform.rotation * Quaternion.Euler(0.0f, 270.0f, 0.0f);
        searchRight = transform.rotation * Quaternion.Euler(0.0f, 90.0f, 0.0f);
    }

    /// <summary>
    /// Runs through the search sequence.
    /// </summary>
    private void searchMode()
    {
        if (searchPhase == 1)
        {
            if ((Time.time - searchStartTime) >= 3)
            {
                searchPhase = 2;
            } else
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, searchLeft, 0.05f);
            }
        } else if (searchPhase == 2)
        {
            if ((Time.time - searchStartTime >= 6))
            {
                searchingForPlayer = false;
                searchModeActive = false;//END AT 6 SECONDS PASSED
            } else
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, searchRight, 0.05f);
            }
        }
    }

    /// <summary>
    /// Check to see if the player is within the Line of Sight of this monster.
    /// </summary>
    /// <returns>Returns true if the player is within LOS or false if the player is not within LOS.</returns>
    private bool checkLOS(float losRatio)
    {
        if (drawLOS)
        {
            float bound = 90 - (losRatio * 90);
            
            Vector3 rightBound = (Quaternion.Euler(0, bound, 0) * transform.forward * 20) + transform.position;
            Vector3 leftBound = (Quaternion.Euler(0, -bound, 0) * transform.forward * 20) + transform.position;
            Debug.DrawLine(transform.position, leftBound, Color.blue);
            Debug.DrawLine(transform.position, rightBound, Color.blue);
        }
        Vector3 playerTarget = (Player.position - transform.position).normalized;

        //TODO: Find a better code structure for returning true or false on a raycast hit.
        if (Vector3.Dot(playerTarget, transform.forward) > losRatio)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, playerTarget, out hit, maxLOS))
            {
                if (hit.transform.CompareTag("Player"))
                {
                    return true;
                }
            } 
        }
        return false;
    }

    /// <summary>
    /// Movement toward the player after they have been detected.
    /// </summary>
    public void playerDetectedMove()
    {
        Vector3 direction = Player.position - transform.position;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction.normalized, out hit)) {

            //NOTE: If the player leaves LOS, we find the Node from world coordinates of the player. If it is an unpathable node, generate neighbors and choose the closest node to the player that is pathable, then A* our way to it. (To simulate "checking")

            if (hit.transform.CompareTag("Player"))
            {

                //OBSTACLE AVOIDANCE: Once we implement obstacles, the monsters will need to detect them. We will use raycasts to check if an obstacle is in the way of us and the player. If it is, we stop moving directly at him and rather turn left or right to evade.
                //How to determine if we turn left or right - we need to think of a way to check the wall the cover is up against. We will always want to turn in the direction opposite that wall (since all of our cover will be up against walls). 
                //Idea: Make cover objects children of the wall they are adjacent to. We can then take the parent of the object detected in RayCasting, use some mathematical magic to determine whether it's to the right or left of the enemy, then turn in the opposite direction.
                //Once we aren't detecting the object in front of us anymore, we know that we have aligned ourselves properly to pass it. We then move forward, checking with rays behind the enemy to detect when we have passed the object in question.
                //After passing, we focus back on the player again.
                //FURTHER NOTES: We need multiple rays to accomplish this. A center ray and then two others that are just slightly bigger than our monster's width.
                //Also, it is critical to check if the player is closer to us than the obstacle we detected. We don't want to just go into evasion mode every time we detect an obstacle.

                //TODO: If we are detecting an object, just move forward, not toward the target. Once we are no longer detecting an object and have passed by the obstacle, lerp back to the target and move to it again.

                rotateTowards(direction.normalized);

                direction.Normalize();
                direction *= speed;

                Rb.velocity = direction;

            } else
            {
                Debug.Log("Player left LOS!");
                playerDetected = false;
                pathfinder.findPathToPlayer(Player.position);
                
            }

        }

    }

    /// <summary>
    /// Movement along a path generated via our A* pathfinder. 
    /// </summary>
    /// <param name="dir">The direction as a Vector3 we must next move to follow along our path</param>
    public void pathfindingMove()
    {
        //No enemy movement until the opening cinematic is completed
        if (!openingScene.openingCinematic.openingComplete) return;
		if (!pathfinder.pathFound) return;

        Vector3 dir = (pathfinder.path[pathfinder.currentWaypoint].worldPosition - transform.position).normalized;

        Debug.DrawLine(transform.position, pathfinder.path[pathfinder.currentWaypoint].worldPosition);

        rotateTowards(dir);

        dir *= speed;
        if (dir.magnitude > speed)
        {
            dir.Normalize();
            dir *= speed;
        }

        Rb.velocity = dir;
        pathfinder.incrementCurrentWaypoint();
    }

    public void rotateTowards(Vector3 dir)
    {
        if (Vector3.Dot(new Vector3(dir.x, transform.position.y, dir.z), transform.forward) < 1f)
        {
            Quaternion newRotation = Quaternion.LookRotation(dir);
            //We are only interested in the Y axis for AI's rotation, so we set the others to 0.
            newRotation.z = 0.0f;
            newRotation.x = 0.0f;
            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, .2f);
        }
    }
}
