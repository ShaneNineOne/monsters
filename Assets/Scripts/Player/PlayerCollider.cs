﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerCollider : MonoBehaviour {


    /// <summary>
    /// Collider for the Player Character
    /// </summary>
    /// 

    // Use this for initialization
    void Start()
    {

    }

    void OnCollisionEnter(Collision c)
    {
        // Check the tag of the colliding object to see if its an enemy
        if (c.collider.tag == "Enemy")
        {
            SceneManager.LoadScene("deathScene");
        }

        // Check the tag of the colliding object to see if its at the goal
        if (c.collider.tag == "Goal" && keyManager.keyManagement.currentKeyCount == keyManager.keyManagement.numOfKeys)
        {
            SceneManager.LoadScene("winScene");
        }
    }

}
