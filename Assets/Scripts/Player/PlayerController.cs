﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    /// <summary>
    /// The players top speed
    /// </summary>
    [SerializeField]
    private float maxMoveSpeed = 5.0f;

    /// <summary>
    /// The speed of our sprinting.
    /// </summary>
    [SerializeField] private float maxSprintSpeed;

    /// <summary>
    /// The current value of our stamina.
    /// </summary>
    [SerializeField] private float staminaBar;

    /// <summary>
    /// The players max turn speed
    /// </summary>
    [SerializeField]
    private float maxTurnSpeed;

    /// <summary>
    /// Are we performing an about faece?
    /// </summary>
    [SerializeField]
    private bool aboutFace = false;

    /// <summary>
    /// Are we sprinting?
    /// </summary>
    [SerializeField]
    private bool sprinting = false;

    /// <summary>
    /// How long does it take to complete an about face rotation action?
    /// </summary>
    private float aboutFaceTimeToComplete = 0.5f;

    /// <summary>
    /// The time of our current point in the about face rotation action.
    /// </summary>
    private float aboutFaceCurrentTime = 0f;

    /// <summary>
    /// The angle that we need to turn to when performing an about face turn.
    /// </summary>
    private Quaternion aboutFaceTurnAngle;

    /// <summary>
    /// How much time does it take to regenerate a point of stamina?
    /// </summary>
    private float staminaDecayTime = 0.05f;

    /// <summary>
    /// What is the current time value of our stamina decay?
    /// </summary>
    private float staminaDecayCurrentTime = 0f;

    /// <summary>
    /// How much time does it take to regenerate a point of stamina?
    /// </summary>
    private float staminaRegenTime = 0.5f;

    /// <summary>
    /// What is the current time value of our stamina regeneration?
    /// </summary>
    private float staminaRegenCurrentTime = 0f;

    /// <summary>
    /// This is used to make our stamina text GUI element look a little better until we get a more proper GUI element in place.
    /// </summary>
    [SerializeField]
    private GUIStyle staminaTextStyle;

    // Update is called once per frame
    void Update()
    {
        //Prevent movement until the opening cinematic is completed.
        if (!openingScene.openingCinematic.openingComplete) return;

        if (Input.GetKeyDown(KeyCode.Space) && !aboutFace)
        {
            aboutFace = true;
            aboutFaceTurnAngle = transform.rotation * Quaternion.Euler(0.0f, 180.0f, 0.0f);
        }

        if (aboutFace)
        {
            aboutFaceTurn();
        }
        else
        {
            move();
        }

        if (!sprinting)
        {
            staminaRegen();
        }
    }

    void OnGUI()
    {
        if (!openingScene.openingCinematic.openingComplete) return;
        GUI.color = new Color(0.4f, 0, 0.8f);
        GUI.Label(new Rect(20, 20, 200, 20), "Stamina: " + staminaBar, staminaTextStyle);
    }

    /// <summary>
    /// Basic player movement controls
    /// </summary>
    private void move()
    {
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * maxTurnSpeed;
        float z;
        if (Input.GetKey(KeyCode.LeftShift) && staminaBar > 0)
        {
            z = Input.GetAxis("Vertical") * Time.deltaTime * maxSprintSpeed;
            if (z != 0)
            {
                sprinting = true;
                staminaDecay();
            }
        }
        else {
            sprinting = false;
            z = Input.GetAxis("Vertical") * Time.deltaTime * maxMoveSpeed;
        }
        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
    }

    /// <summary>
    /// Performs an about face turn (Turns us very rapidly 180 degrees)
    /// </summary>
    private void aboutFaceTurn()
    {
        aboutFaceCurrentTime += Time.deltaTime;
        float lerpPercentage = (aboutFaceCurrentTime / aboutFaceTimeToComplete);//Smooth lerping across timeframe

        transform.rotation = Quaternion.Lerp(transform.rotation, aboutFaceTurnAngle, lerpPercentage);

        if (lerpPercentage >= 1.0f)
        {
            aboutFaceCurrentTime = 0f;
            aboutFace = false;
        }
    }

    /// <summary>
    /// Decays the stamina bar while we are sprinting. 
    /// </summary>
    private void staminaDecay()
    {
        staminaDecayCurrentTime += Time.deltaTime;
        float decayPercentage = staminaDecayCurrentTime / staminaDecayTime;

        if (decayPercentage >= 1.0f && staminaBar > 0)
        {
            staminaDecayCurrentTime = 0;
            staminaBar -= 1;
        }
    }

    /// <summary>
    /// Regenerates the stamina bar while we are not sprinting.
    /// </summary>
    private void staminaRegen()
    {
        staminaRegenCurrentTime += Time.deltaTime;
        float regenPercentage = staminaRegenCurrentTime / staminaRegenTime;

        if (regenPercentage >= 1.0f && staminaBar < 100)
        {
            staminaRegenCurrentTime = 0;
            staminaBar += 1;
        }
    }
}
